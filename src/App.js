import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Map from './components/map';
import Search from './components/search';
import ObjectInfo from './components/object';
import Gallery from './components/gallery'
import Header from './components/header'
import Order from './components/order'
import Popup from './components/popup'
import ListObjects from './components/listobjects'
import * as objectsActions from './actions/objects';



class App extends Component {

    static propTypes = {
        fetchObjects: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.props.fetchObjects();
    }

    render() {
        return (
            <section className='map'>
                <Map />
                <Header />
                <Search />
                <ObjectInfo />
                <ListObjects />
                <Order />
                <Gallery />
                <Popup open={true}>fdg</Popup>
            </section>
        );
    }
}


function mapDispatchToProps(dispatch) {
    return {
        fetchObjects: bindActionCreators(objectsActions, dispatch).fetchObjects,
    }
}
export default connect(null, mapDispatchToProps)(App);
