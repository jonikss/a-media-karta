export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const ADDING_TO_CART_START = 'ADDING_TO_CART_START';
export const ADDING_TO_CART_END = 'ADDING_TO_CART_END';
export const CLEAR_CART = 'CLEAR_CART';
let timerId;

export const addToCart = item => (dispatch, getState) => {

    dispatch({
        type: ADD_TO_CART,
        item
    });

    const { cart } = getState();
    localStorage.setItem('cart.items', JSON.stringify(cart.items));

    if(timerId) {
        clearTimeout(timerId);
        dispatch({
            type: ADDING_TO_CART_END
        });

        setTimeout(() => {
            dispatch({
                type: ADDING_TO_CART_START,
                lastItem:item
            });
        }, 200);

        timerId = setTimeout(() => {
            timerId = undefined;
            dispatch({
                type: ADDING_TO_CART_END
            });
        }, 1200);

    } else {
        dispatch({
            type: ADDING_TO_CART_START,
            lastItem:item
        });

        timerId = setTimeout(() => {
            timerId = undefined;
            dispatch({
                type: ADDING_TO_CART_END
            });
        }, 2000);
    }


}


export const removeFromCart = item => (dispatch, getState) => {
    dispatch({
        type: REMOVE_FROM_CART,
        item
    });
    const { cart } = getState();
    localStorage.setItem('cart.items', JSON.stringify(cart.items));
}


export const clearCart = item => (dispatch, getState) => {
    dispatch({
        type: CLEAR_CART
    });
    localStorage.removeItem('cart.items');
}
