export const FETCH_IMAGES = 'FETCH_IMAGES';
export const OPEN_GALLERY = 'OPEN_GALLERY';
export const CLOSE_GALLERY = 'CLOSE_GALLERY';

export const  fetchImages = items => {
    return {
        type: FETCH_IMAGES,
        items: items
    }
}

export const  openGallery = (index = 0) => {
    return {
        type: OPEN_GALLERY,
        index: index
    }
}

export const  closeGallery = () => {
    return {
        type: CLOSE_GALLERY
    }
}
