export const FETCH_OBJECTS_REQUEST = 'FETCH_OBJECTS_REQUEST';
export const FETCH_OBJECTS_FAILURE = 'FETCH_OBJECTS_FAILURE';
export const FETCH_OBJECTS_SUCCESS = 'FETCH_OBJECTS_SUCCESS';
export const FILTER_TYPES = 'FILTER_TYPES';
export const FILTER_DISTRICTS = 'FILTER_DISTRICTS';
export const SELECT_OBJECT = 'SELECT_OBJECT';
export const OPEN_OBJECT_INFO = 'OPEN_OBJECT_INFO';
export const CLOSE_OBJECT_INFO = 'CLOSE_OBJECT_INFO';
export const OPEN_OBJECTS_LIST = 'OPEN_OBJECTS_LIST';
export const CLOSE_OBJECTS_LIST = 'CLOSE_OBJECTS_LIST';



export const  fetchObjects = () => dispatch => {
    fetch('/data2.json')
    .then(response => {
        dispatch({
            type: FETCH_OBJECTS_REQUEST
        })
        return response.json();
    })
    .then(data => {
        setTimeout(()=>{
            dispatch({
                type: FETCH_OBJECTS_SUCCESS,
                items: data,
                error: ''
            });
        },0);

    })
    .catch( error => {
        dispatch({
            type: FETCH_OBJECTS_FAILURE,
            items: [],
            error: error
        })
    });
}


export const  filterTypes = types => ({
    type: FILTER_TYPES,
    types
});

export const  filterDistricts = districts => ({
    type: FILTER_DISTRICTS,
    districts
});

export const  selectObject = id => ({
    type: SELECT_OBJECT,
    id
});

export const  openObjectInfo = () => ({
    type: OPEN_OBJECT_INFO
});

export const  closeObjectInfo = () => ({
    type: CLOSE_OBJECT_INFO
});

export const  openObjectsList = () => ({
    type: OPEN_OBJECTS_LIST
});

export const  closeObjectsList = () => ({
    type: CLOSE_OBJECTS_LIST
});
