export const OPEN_ORDER = 'OPEN_ORDER';
export const CLOSE_ORDER = 'CLOSE_ORDER';


export const openOrder = () => {
  return {
      type: OPEN_ORDER
  };
}

export const closeOrder = () => {
  return {
      type: CLOSE_ORDER
  };
}
