import React from 'react';

const FormInput = ({name, label, state,handleUserInput, value, errorClass,  ...props}) => {
    return (<div className="form__elem form__elem--input">
                <label htmlFor="name" className={errorClass}>{label}</label>
                <div className="form__field">
                    <input onChange={handleUserInput} value={value} placeholder={label} id={name} name={name} type="text" className="form__input" {...props} />
                </div>
            </div>);
};

export default FormInput;
