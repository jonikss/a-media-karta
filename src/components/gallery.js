import React, {Component, PropTypes} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {PhotoSwipe} from 'react-photoswipe';
import 'react-photoswipe/lib/photoswipe.css';
import * as galleryActions from '../actions/gallery';



class MyGallery extends Component {

    static propTypes = {
        closeGallery: PropTypes.func.isRequired,
        gallery: PropTypes.shape({
            open: React.PropTypes.bool.isRequired,
            index: React.PropTypes.number.isRequired,
            items: React.PropTypes.array.isRequired,
        }),
    }

    static defaultProps = {
        gallery: {
            open: false,
            index: 0,
            items: []
        }
    }

    render() {
        const {gallery, closeGallery} = this.props;
        return(
            <PhotoSwipe
              isOpen={gallery.open}
              items={gallery.items}
              options={{index:gallery.index}}
              onClose={closeGallery}
            />
        );
    }
}

function mapStateToProps(state, props) {
    return {
        gallery: state.gallery
    };
}
function mapDispatchToProps(dispatch) {
    const {closeGallery} = bindActionCreators(galleryActions, dispatch);
    return {closeGallery};
}
export default connect(mapStateToProps, mapDispatchToProps)(MyGallery);
