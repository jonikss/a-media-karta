import React, {Component, PropTypes} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as objectsActions from '../actions/objects';
import * as orderActions from '../actions/order';


class Header extends Component {

    static propTypes = {
        openObjectsList: PropTypes.func.isRequired,
        openOrder: PropTypes.func.isRequired,
        cart: PropTypes.shape({
            adding: PropTypes.bool.isRequired,
            lastItem: PropTypes.object,
            count: PropTypes.number.object
        }),
    }

    static defaultProps = {
        cart: {
            adding: false,
            lastItem: {},
            count: 0
        }
    }

    handleClickList = e => {
        e.preventDefault();
        this.props.openObjectsList();
    }

    openOrder = e => {
        e.preventDefault();
        if(this.props.cart.count !== 0) {
            this.props.openOrder();
        }
    }

    render() {
        const {cart} = this.props;
        let toolTipClasses = cart.adding ? 'objects-count__tooltip objects-count__tooltip--show' : 'objects-count__tooltip';

        let addedItemAddress, addedItemName;
        if(cart.lastItem) {
            addedItemAddress = cart.lastItem['address'];
            addedItemName = cart.lastItem['name'];
        }

        return (
            <header className="map__header">
                <div className="map__wrap">
                    <div className="map__row">
                        <div className="map__col map__col--back">
                            <div className="map__wrap-col"><a href="" className="map__back">Вернуться на сайт</a>
                            </div>
                        </div>
                        <div className="map__col map__col--objects">
                            <div className="map__wrap-col"><a href="" onClick={this.handleClickList} className="map__objects">Список объектов</a>
                            </div>
                        </div>
                        <div className="map__col map__col--order">
                            <div className="map__wrap-col">
                                <div className="map__row map__row--order">
                                    <div className="map__col map__col--count">
                                        <div className="map__wrap-col">
                                            <div className="objects-count"><a className="objects-count__link">Выбрано объектов:    <span>{cart.count}</span></a>
                                                <div className={toolTipClasses}>
                                                    <p className="objects-count__message">Объект добавлен</p>
                                                    <p className="objects-count__name">{addedItemName}</p>
                                                    <p className="objects-count__name">{addedItemAddress}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="map__col map__col--btn">
                                        <div className="map__wrap-col"><a href="" onClick={this.openOrder} disabled={cart.count === 0} className="map__btn">Оформить заявку</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>);
    }
}


function mapStateToProps(state, props) {
    return {
        cart: state.cart,
    };
}
function mapDispatchToProps(dispatch) {
    const {openObjectsList} = bindActionCreators(objectsActions, dispatch);
    const {openOrder} = bindActionCreators(orderActions, dispatch);
    return {
        openObjectsList,
        openOrder
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header);
