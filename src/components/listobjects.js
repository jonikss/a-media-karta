import React, {Component, PropTypes} from 'react';
import Masonry from 'masonry-layout';
import { Scrollbars } from 'react-custom-scrollbars';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as objectsActions from '../actions/objects';


class ListObjects extends Component {

    static propTypes = {
        closeObjectsList: PropTypes.func.isRequired,
        openObjectInfo: PropTypes.func.isRequired,
        selectObject: PropTypes.func.isRequired,
        objects: PropTypes.shape({
            items: PropTypes.array.isRequired,
            filtredItems: PropTypes.array.isRequired,
            typeFilter: PropTypes.array.isRequired,
            districtFilter: PropTypes.array.isRequired,
            selectedObject: PropTypes.number.isRequired,
            openObject: PropTypes.bool.isRequired,
            openList: PropTypes.bool.isRequired
        }),
    }

    static defaultProps = {
        objects: {
            items: [],
            filtredItems: [],
            typeFilter: [],
            districtFilter: [],
            selectedObject: 0,
            openObject: false,
            openList: false
        }
    }

    componentDidUpdate() {
        this.masonry = new Masonry( this.row, {
            itemSelector: '.objects-list__col',
            percentPosition: true,
            transitionDuration: 0
        });
    }

    itemClick = id => e => {
        e.preventDefault();
        this.props.closeObjectsList();
        this.props.openObjectInfo();
        this.props.selectObject(id);
    }

    closeObjectsList = e => {
        e.preventDefault();
        this.props.closeObjectsList();
    }

    render() {
        const {objects} = this.props;
        let classObjectsList = objects.openList ? 'objects-list objects-list--show' : 'objects-list';

        const treeList = objects.items.reduce((prev, item) => {
            if(prev[item.district]) {
                prev[item.district].items.push(item);
            } else {
                prev[item.district] = {items:[item]};
            }
            return prev;
        }, {});
        const listRender = Object.keys(treeList).map((district, i) => {
            return (<div key={i} className="objects-list__col" >
                <div className="objects-list__district">
                    <p className="objects-list__district-title">{district}</p>
                    <div className="objects-list__items">
                        {treeList[district].items.map(item => {
                            const typesString = item.types.map(type => type.name).join(', ');
                            return (<a onClick={this.itemClick(item.id)} key={item.id} href="" className="objects-list__item">
                                <p className="search__item-adress">{item.address}</p>
                                <p className="search__item-name"><b>Типы:</b> {typesString}</p>
                            </a>);
                        })}

                    </div>
                </div>
            </div>);
        });
        return (
            <aside className={classObjectsList}><a onClick={this.closeObjectsList} href="" className="objects-list__close"></a>
            <Scrollbars>
            <div className="objects-list__wrap">
                <div className="objects-list__container">
                    <div ref={ref => {  this.row = ref}} className="objects-list__row">
                        {listRender}
                    </div>
                </div>
            </div>
            </Scrollbars>
        </aside>
        );
    }
}


function mapStateToProps(state, props) {
    return {
        objects: state.objects
    };
}
function mapDispatchToProps(dispatch) {
    const {closeObjectsList, openObjectInfo, selectObject} = bindActionCreators(objectsActions, dispatch);
    return {closeObjectsList, openObjectInfo, selectObject};
}
export default connect(mapStateToProps, mapDispatchToProps)(ListObjects);
