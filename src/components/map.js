import React, {Component, PropTypes} from 'react';
import { YMaps, Map, ObjectManager } from 'react-yandex-maps';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as objectsActions from '../actions/objects';
import markerChecked from '../images/marker-checked.png';
import markerPink from '../images/marker-pink.png';
import markerBlue from '../images/marker-blue.png';
import clusterBlue from '../images/cluster-blue.png';

const mapState = { center: [55.76, 37.64], zoom: 10, margin: [60, 0, 0, 420] };

class MyMap extends Component {


    static propTypes = {
        closeObjectInfo: PropTypes.func.isRequired,
        openObjectInfo: PropTypes.func.isRequired,
        selectObject: PropTypes.func.isRequired,
        objects: PropTypes.shape({
            items: PropTypes.array.isRequired,
            filtredItems: PropTypes.array.isRequired,
            typeFilter: PropTypes.array.isRequired,
            districtFilter: PropTypes.array.isRequired,
            selectedObject: PropTypes.number.isRequired,
            openObject: PropTypes.bool.isRequired,
            openList: PropTypes.bool.isRequired
        }),
        cart: PropTypes.shape({
            adding: PropTypes.bool.isRequired,
            lastItem: PropTypes.object,
        })
    }

    static defaultProps = {
        objects: {
            items: [],
            filtredItems: [],
            typeFilter: [],
            districtFilter: [],
            selectedObject: 0,
            openObject: false,
            openList: false
        },
        cart: {
            adding: false,
            lastItem: {}
        }
    }

    constructor(props) {
        super(props);
        this.state = {ymaps: null};
    }

    componentDidMount = () => {
        //console.log(this.managerInstance);
    }

    componentDidUpdate = () => {
        if(this.managerInstance) {
            const {objects, cart} = this.props;
            const map = this.managerInstance.getMap()
            objects.items.forEach(item => {
                let icon = markerBlue;
                if(Object.keys(cart.items).includes(item.id.toString())) {
                    icon = markerPink;
                }
                if(objects.selectedObject === item.id) {
                    icon = markerChecked;
                    map.setCenter(item.coord, 13, {duration: 800, useMapMargin:true});
                }

                if(this.timer) {
                    clearTimeout(this.timer);
                }

                if(objects.selectedObject === 0) {
                    this.timer = setTimeout(() => {
                        this.timer = undefined;
                        map.setCenter(mapState.center, mapState.zoom, {duration: 800, useMapMargin:true});
                    }, 800);
                }

                this.managerInstance.objects.setObjectOptions(item.id, {
                    iconImageHref: icon
                });
            });

        }
    }

    markerClick = e => {
        var objectId = e.get('objectId');
        const {closeObjectInfo, openObjectInfo, selectObject} = this.props;
        const {objects} = this.props;
        if(this.managerInstance.objects.getById(objectId)){
            if(objects.openObject) {
                closeObjectInfo();
                setTimeout(() => {
                    selectObject(objectId);
                    openObjectInfo();
                },500)
            //this.props.openObjectInfo();
            } else {
                openObjectInfo();
                selectObject(objectId);
            }
        }
    }

    render() {
        const {objects, cart} = this.props;
        let features = objects.filtredItems.map((item) => {
            let icon = markerBlue;
            if(Object.keys(cart.items).includes(item.id.toString())) {
                icon = markerPink;
            } else if(objects.selectedObject === item.id) {
                icon = markerChecked;
            }
            return {
                type: 'Feature',
                id:item.id,
                geometry: {
                    type:'Point',
                    coordinates:item.coord
                },
                properties: {
                    //balloonContent: item.address,
                    clusterCaption: 'Еще одна метка',
                    hintContent: 'Текст подсказки'
                },
                options: {
                    iconLayout: 'default#image',
                    iconImageHref: icon,
                    iconImageSize: [25, 25],
                    iconImageOffset: [-16, -16]
                }
            };
        });
        return (
            <YMaps onApiAvaliable={ ymaps => this.setState({ymaps:ymaps}) }>
                <Map className='map__map' state={mapState} width={'100%'} height={'100%'}>
                    <ObjectManager
                        instanceRef={obj => this.managerInstance = obj}
                        options={{
                          clusterize: true,
                          gridSize: 32,
                        }}
                        objects={{
                          preset: 'islands#greenDotIcon',
                        }}
                        clusters={{
                          preset: 'islands#greenClusterIcons',
                          clusterIcons: [
                              {
                                href: clusterBlue,
                                size: [40, 40],
                                offset: [-16, -16]
                              }
                          ],
                          clusterIconContentLayout: this.state.ymaps ? this.state.ymaps.templateLayoutFactory.createClass('<span class="cluster-count">{{properties.geoObjects.length}}</span> ') : null
                        }}
                        features={features}
                        onMouseEnter={this.onObjectEvent}
                        onMouseLeave={this.onObjectEvent}
                        onClick={this.markerClick}
                    />
                </Map>
            </YMaps>
        );
    }
}


function mapStateToProps(state, props) {
    return {
        objects: state.objects,
        cart: state.cart,
    };
}
function mapDispatchToProps(dispatch) {
    const {closeObjectInfo, openObjectInfo, selectObject} =  bindActionCreators(objectsActions, dispatch);
    return {closeObjectInfo, openObjectInfo, selectObject};
}
export default connect(mapStateToProps, mapDispatchToProps)(MyMap);
