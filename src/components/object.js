import React, { Component, PropTypes } from 'react';
import 'react-select/dist/react-select.css';
import { Scrollbars } from 'react-custom-scrollbars';
import Thumbs from './thumbs';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as objectsActions from '../actions/objects';
import * as cartActions from '../actions/cart';
import { getTypesOfSelectedObject } from '../selectors'


class ObjectInfo extends Component {

    static propTypes = {
        addToCart: PropTypes.func.isRequired,
        removeFromCart: PropTypes.func.isRequired,
        closeObjectInfo: PropTypes.func.isRequired,
        types: PropTypes.arrayOf(React.PropTypes.shape({
            objectId: PropTypes.number.isRequired,
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            images: PropTypes.array.isRequired,
            desc: PropTypes.string.isRequired,
            address:  PropTypes.string.isRequired,
            inCart: PropTypes.bool.isRequired
        }))
    }

    static defaultProps = {
        types: [{
            objectId: 0,
            id: 0,
            name: '',
            images: [],
            desc: '',
            address:  '',
            inCart: false
        }]
    }


    closeObject = e => {
        e.preventDefault();
        this.props.closeObjectInfo();
    }

    addToCart = item => e => {
        e.preventDefault();
        this.props.addToCart(item);
    }

    removeFromCart = itemId => typeId => e => {
        e.preventDefault();
        this.props.removeFromCart({itemId:itemId, typeId:typeId});
    }

    renderTypes(types) {
        return types.map(type => {
            const item = {
                   itemId: type.objectId,
                   typeId: type.id,
                   address: type.address,
                   name: type.name
               };
            return <div key={type.id} className="object__type">
                        <p className="object__type-title">{type.name}</p>
                        {type.images ? <Thumbs images={type.images} /> : ''}
                        {
                            type.inCart
                            ?
                                <a onClick={this.removeFromCart(type.objectId)(type.id)} href="" className="object__type-chose">Убрать</a>
                            :
                                <a onClick={this.addToCart(item)} href="" className="object__type-chose">Выбрать</a>
                        }
                        <div className="object__desc text">
                            <p>{type.desc}</p>
                        </div>
                    </div>;
        });
    }

    render() {
        const { types, openObject } =  this.props;
        return (
            types ? <aside className={openObject ? 'object object--show' : 'object'}>
            <a href="" onClick={this.closeObject} className="object__close"></a>
            <Scrollbars>
                <div className="object__wrap">
                    <p className="object__title">{types.length>0 && types[0].address}</p>
                    <div className="object__types">
                        {types.length>0 && this.renderTypes(types)}
                    </div>
                </div>
            </Scrollbars>
            </aside> : <aside className={openObject ? 'object object--show' : 'object'} ><a href="" onClick={this.closeObject} className="object__close"></a></aside>
        );
    }
}



function mapStateToProps(state, props) {

    const types = getTypesOfSelectedObject(state);
    return {
        openObject:state.objects.openObject,
        types
    };
}
function mapDispatchToProps(dispatch) {
    const { addToCart, removeFromCart } = bindActionCreators(cartActions, dispatch);
    const { closeObjectInfo } = bindActionCreators(objectsActions, dispatch);
    return {
        addToCart,
        removeFromCart,
        closeObjectInfo
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ObjectInfo);
