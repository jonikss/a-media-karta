import React, {Component, PropTypes} from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as cartActions from '../actions/cart';
import * as orderActions from '../actions/order';
import { getObjectsInCart } from '../selectors'
import FormInput from './forminput'


class Order extends Component {

    static propTypes = {
        removeFromCart: PropTypes.func.isRequired,
        closeOrder: PropTypes.func.isRequired,
        clearCart: PropTypes.func.isRequired,
        objectsInCart: PropTypes.arrayOf(React.PropTypes.shape({
            id: PropTypes.number.isRequired,
            typeId: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            images: PropTypes.array,
            address:  PropTypes.string.isRequired
        }))
    }


    static defaultProps = {
        types: [{
            objectId: 0,
            id: 0,
            name: '',
            images: [],
            desc: '',
            address:  '',
            inCart: false
        }]
    }

    constructor (props) {
        super(props);
        this.state = {
            statusForm: 'default',
            name: '',
            email: '',
            phone: '',
            company: '',
            comments: '',
            formErrors: {name:'', email: '',  phone: '', company: '', comments: ''},
            nameValid: false,
            emailValid: false,
            phoneValid: false,
            companyValid: false,
            commentsValid: false,
            formValid: false
        };
    }

    closeOrder = e => {
        e.preventDefault();
        this.props.closeOrder();
        setTimeout(()=>{
            this.setState({statusForm:'defauld'});
        }, 500);
    }

    handleUserInput = e => {
        const {name, value} = e.target;
        this.setState({[name]: value}, () => {
            this.validateField(name, value);
        });
    }

    validateField = (fieldName, value) => {

        let {formErrors, nameValid, emailValid, phoneValid, companyValid, commentsValid, formValid} = this.state;

        switch(fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                formErrors.email = emailValid ? '' : 'Нужно ввести правильный e-mail';
                break;
            case 'name':
                nameValid = value.length >= 2;
                formErrors.name = nameValid ? '': 'Слишком короткое имя';
                break;
            case 'phone':
                phoneValid = value.length >= 2 && value.match(/^[\+]?[0-9]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,5}$/im);
                formErrors.phone = phoneValid ? '': 'Не корректный номер телефона';
                break;
            case 'company':
                companyValid = value.length >= 2;
                formErrors.company = companyValid ? '': 'Слишком короткое название компании';
                break;

            default:
              break;
        }

        this.setState({formErrors, nameValid, emailValid, phoneValid, companyValid, commentsValid, formValid}, this.validateForm);
    }
    validateForm = () => {
      this.setState({formValid: this.state.emailValid && this.state.nameValid && this.state.phoneValid});
    }

    errorClass = error => error === '' ? '' : 'form__error';

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({statusForm:'sending'});
        setTimeout(()=>{
            this.setState({
                statusForm: 'sended',
                name: '',
                email: '',
                phone: '',
                company: '',
                comments: '',
                nameValid: false,
                emailValid: false,
                phoneValid: false,
                companyValid: false,
                commentsValid: false,
                formValid: false
            });
            setTimeout(()=>{
                this.props.clearCart();
            }, 500);

        }, 3000);
    }

    removeObject = itemId => typeId => e => {
        const { objectsInCart, removeFromCart } = this.props;
        e.preventDefault();
        removeFromCart({itemId:itemId, typeId:typeId});
        if(objectsInCart.length === 1) {
            this.props.closeOrder();
        }
    }

    objectsInCartRender(objects) {
        return objects.map(object => {
            const {id, typeId, name, address} = object;
            return  <div key={id + '-' + typeId} className="objects-form__item">
                        <p className="objects-form__name">{name}</p>
                        <p className="objects-form__adress">{address}</p>
                        <a href="" onClick={this.removeObject(id)(typeId)} className="objects-form__remove"></a>
                    </div>;
        });
    }

    render() {
        const { objectsInCart, openOrder } = this.props;
        let modalClass = openOrder ? "modal modal--open" : "modal";
        let modalFormClass = 'modalform';
        if (this.state.statusForm === 'sending') {
            modalFormClass = 'modalform modalform--sending';
        } else if(this.state.statusForm === 'sended') {
            modalFormClass = 'modalform modalform--sended';
        }
        return (
                <aside id="order" className={modalClass}>
                <div onClick={this.closeOrder} className="modal__overlay"></div>
                <div className="modal__content"><span onClick={this.closeOrder} className="modal__close"></span>
                    <div className={modalFormClass}>
                        <div className='modalform__status-sended'>
                            <main className="modalform__main">
                                <h6>Ваш заказа отправлен на обработку</h6>
                                <p className="reset__button"><a href="" onClick={this.closeOrder} className="btn">Продолжить работу с сайтом</a></p>
                            </main>
                        </div>
                        <div className='modalform__status-send'>
                            <span className='modalform__loader'></span>
                            <form className="form" onSubmit={this.handleSubmit}>
                                <div className="objects-form">
                                    <p className="objects-form__title">Выбранные объекты<span className="objects-form__count">{objectsInCart && objectsInCart.length}</span>
                                    </p>
                                    <div className="objects-form__items">
                                        {objectsInCart && this.objectsInCartRender(objectsInCart)}
                                    </div>
                                </div>


                                <header className="modalform__header">
                                    <div className="modalform__header-left">
                                        <p className="modalform__title">Оформление заявки
                                        </p>
                                    </div>
                                </header>
                                <main className="modalform__main">
                                    <div className="modalform__desc text">
                                        <p>Иструкция к эксплуатации формы. Эмпирическая история искусств, в том числе, представляет собой классицизм.</p>
                                    </div>
                                    <FormInput name={'name'} label={'Ваше Имя *'} value={this.state.name} errorClass={this.errorClass(this.state.formErrors.name)} handleUserInput={this.handleUserInput} />

                                    <FormInput name={'email'} label={'Ваш e-mail *'} value={this.state.email} errorClass={this.errorClass(this.state.formErrors.email)} handleUserInput={this.handleUserInput} />

                                    <FormInput name={'phone'} label={'Номер мобильного телефона *'} value={this.state.phone} errorClass={this.errorClass(this.state.formErrors.phone)} handleUserInput={this.handleUserInput} />

                                    <FormInput name={'company'} label={'Компания (не обязательно)'} value={this.state.company} errorClass={this.errorClass(this.state.formErrors.company)} handleUserInput={this.handleUserInput} />


                                    <div className="form__elem form__elem--textarea">
                                        <label htmlFor="comments" className={this.errorClass(this.state.formErrors.comments)}>Ваше сообщение</label>
                                        <div className="form__field">
                                            <textarea onChange={this.handleUserInput} value={this.state.comments} placeholder="Ваше сообщение" id="comments" name="comments" className="form__textarea"></textarea>
                                        </div>
                                    </div>
                                    <div className="modalform__note">
                                        <p>Поля, отмеченные "звездочкой", обязательны для заполнения. Менеджер перезвонит вам в течение 5 минут.</p>
                                    </div>
                                    <div id="js-form__error_box-callback" className="form__note h-nomargins">
                                        {Object.keys(this.state.formErrors).map((fieldName, i) => {
                                          if(this.state.formErrors[fieldName].length > 0){
                                            return (
                                              <p className="form__error" key={i}>{this.state.formErrors[fieldName]}</p>
                                            )
                                          } else {
                                            return '';
                                          }
                                        })}
                                    </div>

                                </main>
                                <footer className="modalform__footer">
                                    <div className="modalform__btns">
                                        <div className="modalform__btns-left">
                                            <button disabled={!this.state.formValid} className="modalform__btn">Отправить
                                            </button>
                                        </div>
                                    </div>
                                </footer>

                            </form>
                        </div>
                    </div>
                </div>
            </aside>
        );
    }
}



function mapStateToProps(state, props) {
    const objectsInCart = getObjectsInCart(state);
    return {
        objectsInCart,
        openOrder: state.order.openOrder
    };
}
function mapDispatchToProps(dispatch) {
    const { closeOrder } = bindActionCreators(orderActions, dispatch);
    const { removeFromCart, clearCart } = bindActionCreators(cartActions, dispatch);
    return {
        removeFromCart,
        closeOrder,
        clearCart
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Order);
