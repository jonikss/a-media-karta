import React, { Component, PropTypes } from 'react';
import 'react-select/dist/react-select.css';
import { Scrollbars } from 'react-custom-scrollbars';

class Popup extends Component {
    render() {
        const { open, modifier } =  this.props;
        let classes = ['popup'];
        open ? classes.push('popup--open'):null;
        modifier ? classes.push('popup--' + modifier):null;
        return (
            <aside className={classes.join(' ')}>
                <a href="" onClick={this.closeObject} className="popup__close"></a>
                <Scrollbars>
                    {this.props.children}
                </Scrollbars>
            </aside>
        );
    }
}

export default Popup;
