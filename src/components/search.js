import React, { Component, PropTypes } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as objectsActions from '../actions/objects';



class Search extends Component {
    static propTypes = {
        openObjectInfo: PropTypes.func.isRequired,
        selectObject: PropTypes.func.isRequired,
        filterDistricts: PropTypes.func.isRequired,
        filterTypes: PropTypes.func.isRequired,
        objects: PropTypes.shape({
            items: PropTypes.array.isRequired,
            filtredItems: PropTypes.array.isRequired,
            typeFilter: PropTypes.array.isRequired,
            districtFilter: PropTypes.array.isRequired,
            selectedObject: PropTypes.number.isRequired,
            openObject: PropTypes.bool.isRequired,
            openList: PropTypes.bool.isRequired
        })
    }

    static defaultProps = {
        objects: {
            items: [],
            filtredItems: [],
            typeFilter: [],
            districtFilter: [],
            selectedObject: 0,
            openObject: false,
            openList: false
        }
    }

    itemClick = id => e => {
        e.preventDefault();
        this.props.openObjectInfo();
        this.props.selectObject(id);
    }



    districtChange = vals => {
        this.props.filterDistricts(vals.split(',').filter(el =>el.length !== 0));
    }
    typeChange = vals => {
        this.props.filterTypes(vals.split(',').filter(el =>el.length !== 0).map(val => Number(val)));
    }


  render() {
    const {objects} = this.props;
    const searchList = objects.filtredItems.map((item) => {
        const typesString = item.types.map(type => type.name).join(', ');
        return <a href="" key={item.id} onClick={this.itemClick(item.id)} className="search__item">
            <p className="search__item-adress">{item.address}</p>
            <p className="search__item-name"><b>Типы:</b> {typesString}</p>
        </a>;
    });
    return (

      <aside className="search">
        <Scrollbars>
            <div className="search__wrap">
                <header className="search__header">
                    <div className="search__logo">
                    </div>
                    <p className="search__title">Интерактивная карта рекламной сети
                    </p>
                </header>
                <main className="search__main">
                <form className="form search__filter">
                        <div className="form__elem-multiselect">
                            <label>Район размещения</label>
                            <Select
                                name="district"
                                value={objects.districtFilter ? objects.districtFilter.join(',') : ''}
                                multi={true}
                                options={[
                                    { label: "Центральный", value: "Центральный"},
                                    { label: "Октябрьский", value: "Октябрьский" },
                                    { label: "Железнодорожный", value: "Железнодорожный" },
                                    { label: "Советский", value: "Советский" },
                                    { label: "Свердловский", value: "Свердловский" },
                                    { label: "Кировский", value: "Кировский"},
                                ]}
                                placeholder='Район размещения'
                                onChange={this.districtChange}
                            />
                        </div>
                        <div className="form__elem-multiselect">
                            <label>Тип конструкции</label>
                            <div className="form__field">
                            <Select
                                name="type"
                                value={objects.typeFilter ? objects.typeFilter.join(',') : ''}
                                multi={true}
                                options={[
                                  { value: 1, label: 'Билборд (6х3 м)', simpleValue: false},
                                  { value: 2, label: 'Сити-формат (1,2х1,8м)' },
                                  { value: 3, label: 'Суперборд' },
                                  { value: 4, label: 'Пиллар' },
                                  { value: 5, label: 'Призматрон' },

                                ]}
                                placeholder='Тип конструкции'
                                onChange={this.typeChange}
                            />
                            </div>
                        </div>
                    </form>
                    <div className="search__items">
                        {searchList}
                    </div>
                </main>
            </div>
        </Scrollbars>
      </aside>

    );
  }
}


function mapStateToProps(state, props) {
    return {
        objects: state.objects
    };
}
function mapDispatchToProps(dispatch) {
    const { openObjectInfo, selectObject, filterDistricts, filterTypes } = bindActionCreators(objectsActions, dispatch);
    return { openObjectInfo, selectObject, filterDistricts, filterTypes };
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);
