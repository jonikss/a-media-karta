import React, {Component, PropTypes} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import 'react-photoswipe/lib/photoswipe.css';
import * as galleryActions from '../actions/gallery';


class Thumbs extends Component {

    static propTypes = {
        fetchImages: PropTypes.func.isRequired,
        openGallery: PropTypes.func.isRequired,
        closeGallery: PropTypes.func.isRequired,
        gallery: PropTypes.shape({
            items: PropTypes.array.isRequired,
            open: PropTypes.bool.isRequired,
            index: PropTypes.number.isRequired,

        })
    }

    static defaultProps = {
        gallery: {
            items: [],
            open: false,
            index: 0,
        }
    }

    handleClick = i => e => {
        e.preventDefault();
        const items = this.props.images.map(image => {
            return {
                src:image.big.src,
                w:image.big.width,
                h:image.big.height
            };
        });
        this.props.fetchImages(items);
        this.props.openGallery(i);
    }
    render() {
        const {images} = this.props;
        const countImages = images.length;
        const pictureItems  = images.map((image, i) => {
            return <div key={i} className="object__pictures-item">
                    <a onClick={this.handleClick(i)} href={image.big.src} className="object__picture"><img src={image.small.src} width={image.small.width} height={image.small.hight} className="object__picture-img" alt="" /></a>
                </div>;
        });

        return(
            <div className="object__pictures">
                {countImages > 4 ? <p className="object__pictures-count">+{countImages - 4 }</p>: <p/>}
                <div className="object__pictures-items">
                    {pictureItems.slice(0,4)}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        gallery: state.gallery
    };
}
function mapDispatchToProps(dispatch) {
    const { fetchImages, openGallery, closeGallery } = bindActionCreators(galleryActions, dispatch);
    return { fetchImages, openGallery, closeGallery };
}

export default connect(mapStateToProps, mapDispatchToProps)(Thumbs);
