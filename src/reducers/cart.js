import {ADD_TO_CART, REMOVE_FROM_CART, ADDING_TO_CART_START, ADDING_TO_CART_END, CLEAR_CART} from '../actions/cart';

const storageItems = JSON.parse(localStorage.getItem('cart.items')) || {};
const storageCount = Object.keys(storageItems).reduce((sum, item) => sum + Object.keys(storageItems[item]).length, 0);
let initialState = {
    items: storageItems,
    //items: {},
    count: storageCount,
    adding: false,
    lastItem: {}
};


export default (state = initialState, payload) => {

    switch (payload.type) {
        case ADD_TO_CART:
            var items = {...state.items};
            var {itemId, typeId} = payload.item;
            if(items[itemId]) {
                let typeIds = {...items[itemId], [typeId]:payload.item};
                items[itemId] = typeIds;
            } else {
                items[itemId] = {[typeId]:payload.item};
            }
            var count = Object.keys(items).reduce((sum, item) => sum + Object.keys(items[item]).length, 0);
            var newState = {...state, items:items, count:count};
            return newState;

        case REMOVE_FROM_CART:
            var items = {...state.items};
            var {itemId, typeId} = payload.item;
            var typeIds =  {...items[itemId]};
            delete typeIds[typeId]
            if(Object.keys(typeIds).length === 0) {
                delete items[itemId];
            } else {
                items[itemId] = typeIds;
            }
            var count = Object.keys(items).reduce((sum, item) => sum + Object.keys(items[item]).length, 0);
            var newState = {...state, items:items, count:count};
            return newState;

        case ADDING_TO_CART_START:
            return {...state, adding: true, lastItem:payload.lastItem};

        case ADDING_TO_CART_END:
            return {...state, adding: false};

        case CLEAR_CART:
            return {
                items: {},
                count: 0,
                adding: false,
                lastItem: {}
            };

        default:
            return state;
    }
};
