import {FETCH_IMAGES, OPEN_GALLERY, CLOSE_GALLERY} from '../actions/gallery'


export default (state = {items:[], open:false, index:0}, payload) => {
    switch (payload.type) {
        case FETCH_IMAGES:
            return {...state, items:payload.items}
        case OPEN_GALLERY:
            return {...state, open:true, index:payload.index}
        case CLOSE_GALLERY:
            return {...state, open:false}
        default:
            return state;
    }
};
