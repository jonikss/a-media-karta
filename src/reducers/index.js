// src/reducers/index.js
import cart from './cart';
import objects from './objects';
import gallery from './gallery';
import order from './order';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    cart,
    objects,
    gallery,
    order
});

export default rootReducer;
