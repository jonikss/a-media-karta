import {FETCH_OBJECTS_REQUEST, FETCH_OBJECTS_FAILURE, FETCH_OBJECTS_SUCCESS, FILTER_TYPES, FILTER_DISTRICTS, SELECT_OBJECT, OPEN_OBJECT_INFO, CLOSE_OBJECT_INFO, OPEN_OBJECTS_LIST, CLOSE_OBJECTS_LIST} from '../actions/objects';


function getFiltred(objects) {
    let filteredTypeItems = [...objects.items];
    if(objects.typeFilter.length > 0) {
        filteredTypeItems = objects.items.filter(item => {
            let res = false;
            item.types.forEach(type => {
                if(objects.typeFilter.includes(type.id)) {
                    res = true;
                }
            });
            return res;
        });
    }

    let filteredDistrictItems = [...objects.items];
    if(objects.districtFilter.length > 0) {
        filteredDistrictItems = objects.items.filter(item => objects.districtFilter.includes(item.district));
    }

    return filteredDistrictItems.filter(el => filteredTypeItems.indexOf(el) !== -1);
}

export default (state = {items:[], filtredItems:[], error:'', status:'', typeFilter:[], districtFilter:[], selectedObject:0, openObject:false, openList:false}, payload) => {
    switch (payload.type) {
        case FETCH_OBJECTS_SUCCESS:
            return {...state, items:payload.items, filtredItems:payload.items}
        case FILTER_DISTRICTS:
            console.log('filtered'); 
            return {
                ...state,
                districtFilter:payload.districts,
                filtredItems:getFiltred({
                    ...state,
                    districtFilter:payload.districts
                })
            }
        case FILTER_TYPES:
            return {...state,
                typeFilter:payload.types,
                filtredItems:getFiltred({
                    ...state,
                    typeFilter:payload.types
                })
            }
        case SELECT_OBJECT:
            return {...state, selectedObject:payload.id}
        case OPEN_OBJECT_INFO:
            return {...state, openObject:true}
        case CLOSE_OBJECT_INFO:
            return {...state, openObject:false, selectedObject: 0}
        case OPEN_OBJECTS_LIST:
            return {...state, openList:true}
        case CLOSE_OBJECTS_LIST:
            return {...state, openList:false}
        default:
            return state;
    }
};
