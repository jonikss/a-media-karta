import {OPEN_ORDER, CLOSE_ORDER} from '../actions/order'

export default (state = {openOrder:false}, payload) => {

    switch (payload.type) {
        case OPEN_ORDER:
            return {...state, openOrder:true};
        case CLOSE_ORDER:
            return {...state, openOrder:false};
        default:
            return state;
    }
};
