export function getTypesOfSelectedObject(state) {
    let object = state.objects.items.find(item => item.id === state.objects.selectedObject);
    if(object) {
        return object.types.map(type => {
            return {
                objectId: object.id,
                id: type.id,
                name: type.name,
                images: type.images,
                desc: type.desc,
                address:object.address,
                inCart: state.cart.items[object.id] && state.cart.items[object.id][type.id] ? true : false
            }
        });
    } else {
        return undefined;
    }
}


export function getObjectsInCart(state) {
    return Object.keys(state.cart.items).reduce((accum, id) => {
        id = Number(id);
        Object.keys(state.cart.items[id]).forEach(typeId => {
            typeId = Number(typeId);
            const {name, address} = state.cart.items[id][typeId];
            accum.push(
                {
                    id,
                    typeId,
                    name,
                    address
                }
            );
        });
        return accum;
    }, []);
}
